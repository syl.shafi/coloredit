import 'dart:math' as math;
import 'dart:ui';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image/image.dart' as img;
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:poly/poly.dart';
import 'package:url_launcher/url_launcher.dart';
import 'image_helper.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(new MyApp());
  });
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Color Edit',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Home'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final picker = ImagePicker();
  img.Image image;
  List<int> bytes;
  Color pickerColor = Color(0xffff0000);
  var imageRGBArray;
  var tempImageRGBArray;
  double allowDistance = 50;
  double filterLevel = 90;
  double adjustment = 0;
  List<List<int>> imageUpdateRecords = new List<List<int>>();
  bool paintMode = false;
  bool isPainted = false;
  bool magicMode = false;
  bool isBlured = false;
  bool blurMode = false;
  bool isSpelled = false;
  bool regionMode = false;
  bool tapMode = false;
  int tapStartX = 0;
  int tapStartY = 0;
  double zoomOriginX = 1;
  double zoomOriginY = 1;
  bool anglerFilter = true;
  bool galarySelect = false;
  int brushSize = 5;
  int currentIndex = 0;
  var tempBytes;
  img.Image tempImage;
  bool enableScroll = true;
  String path;
  int currentPolyPoint = -1;
  List<Point<num>> regionPoints = new List<Point<num>>();

  void changeColor(Color color) {
    setState(() => pickerColor = color);
  }

  _onTapDown(var details, BuildContext context) async {
    double appBarHeight = appBar.preferredSize.height;

    var x = details.globalPosition.dx + zoomOriginX - 1;
    var y = details.globalPosition.dy -
        appBarHeight -
        MediaQuery.of(context).padding.top +
        zoomOriginY -
        1;

    if (regionMode) {
      currentPolyPoint = -1;

      if (regionPoints.length == 0) {
        regionPoints.add(math.Point(x, y));
        currentPolyPoint = regionPoints.length - 1;
      } else {
        double distance = math.sqrt(
            ((x - regionPoints[0].x) * (x - regionPoints[0].x)) +
                ((y - regionPoints[0].y) * (y - regionPoints[0].y)));
        int tempCurrentPolyPoint = 0;

        for (int i = 0; i < regionPoints.length; i++) {
          double currDistance = math.sqrt(
              ((x - regionPoints[i].x) * (x - regionPoints[i].x)) +
                  ((y - regionPoints[i].y) * (y - regionPoints[i].y)));

          if (distance > currDistance) {
            distance = currDistance;
            tempCurrentPolyPoint = i;
          }
        }

        if (distance > brushSize + 10) {
          regionPoints.add(math.Point(x, y));
          currentPolyPoint = regionPoints.length - 1;
        } else {
          currentPolyPoint = tempCurrentPolyPoint;
        }
      }

      drawRegion(x, y);
    }
  }

  _onTapUpdateImage(var details, BuildContext context) async {
    double appBarHeight = appBar.preferredSize.height;

    var x = details.globalPosition.dx + zoomOriginX - 1;
    var y = details.globalPosition.dy -
        appBarHeight -
        MediaQuery.of(context).padding.top +
        zoomOriginY -
        1;

    if (regionMode) {
      drawRegion(x.toInt() * 1.0, y.toInt() * 1.0);
    } else if (paintMode) {
      if (!isPainted) {
        isPainted = true;
        imageUpdateRecords.add(ImageHelper.getFullImageByets(image));
      }

      replaceColorAt(x.toInt(), y.toInt(), pickerColor);
    } else if (magicMode) {
      if (!isSpelled) {
        isSpelled = true;
        imageUpdateRecords.add(ImageHelper.getFullImageByets(image));
      }

      applySpellAt(x.toInt(), y.toInt());
    } else if (blurMode) {
      if (!isBlured) {
        isBlured = true;
        imageUpdateRecords.add(ImageHelper.getFullImageByets(image));
      }

      applyGaussianBlurAt(x.toInt(), y.toInt());
    } else if (!regionMode && !paintMode && !magicMode && !blurMode) {
      int selectedPixel = image.getPixelSafe(x.toInt(), y.toInt());

      //print(selectedPixel);

      imageUpdateRecords.add(ImageHelper.getFullImageByets(image));
      replaceColor(selectedPixel, pickerColor);
    }
  }

  void drawRegion(double x, double y) {
    Map<String, dynamic> data = ImageHelper.drawRegion(
        x, y, image, imageRGBArray, currentPolyPoint, regionPoints, brushSize);

    image = data["image"];
    regionPoints = data["regionPoints"];

    setState(() {});
  }

  void applySpellAt(int x, int y) {
    Map<String, dynamic> data = ImageHelper.applySpellAt(
        x, y, brushSize, image, tempImageRGBArray, imageRGBArray, regionPoints);

    image = data["image"];
    imageRGBArray = data["imageRGBArray"];

    setState(() {});
  }

  void applyGaussianBlurAt(int x, int y) {
    Map<String, dynamic> data = ImageHelper.applyGaussianBlurAt(
        x, y, image, imageRGBArray, regionPoints);

    image = data["image"];
    imageRGBArray = data["imageRGBArray"];

    setState(() {});
  }

  void replaceColorAt(int x, int y, Color currentColor) {
    Map<String, dynamic> data = ImageHelper.replaceColorAt(
        x,
        y,
        currentColor,
        brushSize,
        image,
        tempImageRGBArray,
        imageRGBArray,
        regionPoints,
        adjustment);

    image = data["image"];
    imageRGBArray = data["imageRGBArray"];

    setState(() {});
  }

  void replaceColor(int selectedPixel, Color currentColor) {
    Map<String, dynamic> data = ImageHelper.replaceColor(
        selectedPixel,
        currentColor,
        image,
        imageRGBArray,
        tempImageRGBArray,
        regionPoints,
        adjustment,
        allowDistance,
        filterLevel,
        anglerFilter);

    image = data["image"];
    imageRGBArray = data["imageRGBArray"];

    setState(() {});
  }

  void zoomImage(double scale, BuildContext context) {
    Map<String, dynamic> data = ImageHelper.zoomImage(
        scale,
        context,
        zoomOriginX,
        zoomOriginY,
        bytes,
        tempBytes,
        image,
        tempImage,
        imageRGBArray,
        tempImageRGBArray,
        regionPoints);

    zoomOriginX = data["zoomOriginX"];
    zoomOriginY = data["zoomOriginY"];
    bytes = data["bytes"];
    tempBytes = data["tempBytes"];
    image = data["image"];
    tempImage = data["tempImage"];
    imageRGBArray = data["imageRGBArray"];
    tempImageRGBArray = data["tempImageRGBArray"];
    regionPoints = data["regionPoints"];            

    setState(() {});
  }

  changePosition(int xDist, int yDist, BuildContext context) {

    Map<String, dynamic> data = ImageHelper.changePosition(xDist, yDist, context, zoomOriginX, zoomOriginY, image);

    zoomOriginX = data["zoomOriginX"];
    zoomOriginY = data["zoomOriginY"];

    setState(() {});
  }

  Future getImage(BuildContext context) async {
    final pickedFile = galarySelect
        ? await picker.getImage(source: ImageSource.gallery)
        : await picker.getImage(source: ImageSource.camera);

    if (pickedFile != null) {
      path = pickedFile.path;

      File croppedFile = await ImageHelper.cropImage(path, context);

      //File croppedFile = new File(path);

      if (croppedFile == null) return;

      zoomOriginX = 1;
      zoomOriginY = 1;

      imageUpdateRecords = new List<List<int>>();

      bytes = await croppedFile.readAsBytes();
      bytes = await FlutterImageCompress.compressWithList(
        bytes,
        minHeight: 1080,
        minWidth: 900,
        quality: 100,
        rotate: 0,
      );

      image = img.decodeImage(bytes);

      image = img.copyResize(image,
          width: MediaQuery.of(context).size.width.toInt(),
          height: (MediaQuery.of(context).size.height * 0.6).toInt());

      imageRGBArray =
          new List.generate(image.width, (_) => new List(image.height));

      for (int i = 0; i < image.width; i++) {
        for (int j = 0; j < image.height; j++) {
          imageRGBArray[i][j] = image.getPixelSafe(i, j);
        }
      }

      //print(imageRGBArray);

      tempBytes = await croppedFile.readAsBytes();
      tempBytes = await FlutterImageCompress.compressWithList(
        tempBytes,
        minHeight: 1080,
        minWidth: 900,
        quality: 100,
        rotate: 0,
      );

      tempImage = img.decodeImage(tempBytes);

      tempImage = img.copyResize(tempImage,
          width: MediaQuery.of(context).size.width.toInt(),
          height: (MediaQuery.of(context).size.height * 0.6).toInt());

      tempImageRGBArray =
          new List.generate(tempImage.width, (_) => new List(tempImage.height));

      for (int i = 0; i < tempImage.width; i++) {
        for (int j = 0; j < tempImage.height; j++) {
          tempImageRGBArray[i][j] = tempImage.getPixelSafe(i, j);
        }
      }
    } else {
      //print('No image selected.');
    }

    setState(() {});
  }

/* 
  Future<void> loadImage() async {
    var byteData = await rootBundle.load('./images/image1.jpg');

    bytes = byteData.buffer.asUint8List();

    image = img.decodeImage(bytes);

    setState(() {});
  }

  */

  void undo() {
    print("length");
    print(imageUpdateRecords.length);
    print("length");

    if (imageUpdateRecords.length > 0) {
      //print("hellow undo");

/*      var oldBytes = imageUpdateRecords[imageUpdateRecords.length - 1];

     oldBytes = await FlutterImageCompress.compressWithList(
        oldBytes,
        minHeight: 1080,
        minWidth: 900,
        quality: 100,
        rotate: 0,
      ); 

     img.Image oldImage = img.decodeImage(oldBytes);
     */

      bytes = imageUpdateRecords[imageUpdateRecords.length - 1];
      img.Image oldImage = img.decodeImage(bytes);
      int oldWidth = oldImage.width;
      int oldHeight = oldImage.height;

      print(zoomOriginX);
      zoomOriginX = (zoomOriginX * (oldWidth / image.width)).round() * 1.0;
      print(zoomOriginX);
      zoomOriginY = (zoomOriginY * (oldHeight / image.height)).round() * 1.0;

      if (regionPoints.length > 0) {
        List<Point<num>> newRegionPoints = new List<Point<num>>();

        for (int i = 0; i < regionPoints.length; i++) {
          int x =
              (regionPoints.elementAt(i).x * (oldWidth / image.width)).toInt();
          int y = (regionPoints.elementAt(i).y * (oldHeight / image.height))
              .toInt();
          newRegionPoints.add(math.Point(x, y));
        }

        regionPoints = newRegionPoints;
      }

      image = img.copyResize(oldImage,
          width: oldImage.width, height: oldImage.height);

      //bytes = img.encodeJpg(image);

      imageRGBArray =
          new List.generate(image.width, (_) => new List(image.height));

      for (int i = 0; i < image.width; i++) {
        for (int j = 0; j < image.height; j++) {
          imageRGBArray[i][j] = image.getPixelSafe(i, j);
        }
      }

      tempImage = img.copyResize(tempImage,
          width: oldImage.width, height: oldImage.height);

      tempBytes = img.encodeJpg(tempImage);

      tempImageRGBArray =
          new List.generate(tempImage.width, (_) => new List(tempImage.height));

      for (int i = 0; i < tempImage.width; i++) {
        for (int j = 0; j < tempImage.height; j++) {
          tempImageRGBArray[i][j] = tempImage.getPixelSafe(i, j);
        }
      }

      imageUpdateRecords.removeAt(imageUpdateRecords.length - 1);

      setState(() {});
    }
  }

  Widget manageScroll(Widget childWidget, bool addScroll) {
    if (addScroll) {
      return SingleChildScrollView(
        child: childWidget,
      );
    }

    return childWidget;
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  var appBar = AppBar(title: Text("Paint World"));

  @override
  void initState() {
    super.initState();

    getImage(context);
  }

  void changeFilterMode() {
    setState(() {
      anglerFilter = !anglerFilter;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar,
      body: manageScroll(
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height -
                appBar.preferredSize.height -
                MediaQuery.of(context).padding.top -
                kBottomNavigationBarHeight,
            child: Column(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  height:
                      (MediaQuery.of(context).size.height * 0.6).toInt() * 1.0,
                  child: bytes != null
                      ? paintMode || magicMode || blurMode
                          ? GestureDetector(
                              child: FittedBox(
                                child: Image.memory(
                                    ImageHelper.getByets(context, image,
                                        zoomOriginX, zoomOriginY),
                                    gaplessPlayback: true),
                                fit: BoxFit.none,
                              ),
                              onPanUpdate: (details) =>
                                  _onTapUpdateImage(details, context),
                            )
                          : regionMode
                              ? GestureDetector(
                                  child: FittedBox(
                                    child: Image.memory(
                                        ImageHelper.getByets(context, image,
                                            zoomOriginX, zoomOriginY),
                                        gaplessPlayback: true),
                                    fit: BoxFit.none,
                                  ),
                                  onTapDown: (details) =>
                                      _onTapDown(details, context),
                                  onPanUpdate: (details) =>
                                      _onTapUpdateImage(details, context),
                                )
                              : tapMode
                                  ? GestureDetector(
                                      onPanStart: (details) {
                                        tapStartX =
                                            details.globalPosition.dx.toInt();
                                        tapStartY =
                                            details.globalPosition.dy.toInt();
                                      },
                                      onPanUpdate: (details) {
                                        int xDist = tapStartX -
                                                    details.globalPosition.dx
                                                        .toInt() >=
                                                0
                                            ? 1
                                            : -1;
                                        int yDist = tapStartY -
                                                    details.globalPosition.dy
                                                        .toInt() >=
                                                0
                                            ? 1
                                            : -1;
                                        changePosition(xDist, yDist, context);
                                      },
                                      child: Padding(
                                          padding: EdgeInsets.all(8),
                                          child: Image.memory(
                                              ImageHelper.getByets(
                                                  context,
                                                  image,
                                                  zoomOriginX,
                                                  zoomOriginY),
                                              gaplessPlayback: true)),
                                    )
                                  : GestureDetector(
                                      child: Padding(
                                        padding: EdgeInsets.all(8),
                                        child: Image.memory(
                                            ImageHelper.getByets(context, image,
                                                zoomOriginX, zoomOriginY),
                                            gaplessPlayback: true),
                                      ),
                                      onTapDown: (TapDownDetails details) =>
                                          _onTapUpdateImage(details, context),
                                    )
                      : Center(child: Text('No Image.')),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.4 -
                      appBar.preferredSize.height -
                      MediaQuery.of(context).padding.top -
                      kBottomNavigationBarHeight -
                      10,
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 15,
                        ),
                        Center(
                          child: Text('Sensitivity'),
                        ),
                        Slider(
                          value: allowDistance,
                          min: 0,
                          max: 100,
                          divisions: 100,
                          label: "Sensitivity : " +
                              allowDistance.round().toString(),
                          onChanged: (double value) {
                            setState(() {
                              allowDistance = value;
                            });
                          },
                        ),
                        anglerFilter
                            ? Center(
                                child: Text('Filter Level'),
                              )
                            : SizedBox(),
                        anglerFilter
                            ? Slider(
                                value: filterLevel,
                                min: 1,
                                max: 100,
                                divisions: 100,
                                label: "Filter Level : " +
                                    filterLevel.round().toString(),
                                onChanged: (double value) {
                                  setState(() {
                                    filterLevel = value;
                                  });
                                },
                              )
                            : SizedBox(),
                        Center(
                          child: Text('adjustment'),
                        ),
                        Slider(
                          value: adjustment,
                          min: -50,
                          max: 50,
                          divisions: 100,
                          label:
                              "adjustment : " + adjustment.round().toString(),
                          onChanged: (double value) {
                            setState(() {
                              adjustment = value;
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          enableScroll),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentIndex,
        selectedItemColor: Colors.amber[800],
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.image,
              size: 0,
            ),
            title: PopupMenuButton<int>(
                icon: Icon(
                  Icons.image,
                ),
                onSelected: (int result) {
                  currentIndex = 0;

                  if (result == 0) {
                    setState(() {
                      galarySelect = false;
                    });
                    getImage(context);
                  }
                  if (result == 1) {
                    setState(() {
                      galarySelect = true;
                    });
                    getImage(context);
                  }
                  if (result == 2) {
                    if (bytes != null) {
                      setState(() {});
                      ImageHelper.saveImage(ImageHelper.getByets(
                          context, image, zoomOriginX, zoomOriginY));
                    }
                  }

                  paintMode = false;
                  isPainted = false;
                  magicMode = false;
                  isSpelled = false;
                  if (regionMode) {
                    undo();
                  }
                  regionMode = false;
                  enableScroll = true;

                  setState(() {});
                },
                itemBuilder: (BuildContext context) => <PopupMenuEntry<int>>[
                      const PopupMenuItem<int>(
                        value: 0,
                        child: Text('Camera'),
                      ),
                      const PopupMenuItem<int>(
                        value: 1,
                        child: Text('Galary'),
                      ),
                      const PopupMenuItem<int>(
                        value: 2,
                        child: Text('Save'),
                      ),
                    ]),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.settings,
              size: 0,
            ),
            title: PopupMenuButton<int>(
                icon: Icon(
                  Icons.settings,
                  size: 30,
                ),
                onSelected: (int index) {
                  currentIndex = 1;

                  if (index == 1) {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          titlePadding: const EdgeInsets.all(0.0),
                          contentPadding: const EdgeInsets.all(0.0),
                          content: Container(
                              height: MediaQuery.of(context).size.height * 0.7,
                              child: SingleChildScrollView(
                                child: Column(
                                  children: <Widget>[
                                    ColorPicker(
                                      pickerColor: pickerColor,
                                      onColorChanged: changeColor,
                                      colorPickerWidth: 300.0,
                                      pickerAreaHeightPercent: 0.7,
                                      enableAlpha: true,
                                      displayThumbColor: true,
                                      paletteType: PaletteType.hsv,
                                    ),
                                  ],
                                ),
                              )),
                          actions: <Widget>[
                            RaisedButton(
                                child: Text('Save'),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                })
                          ],
                        );
                      },
                    );
                  }
                  if (index == 2) {
                    paintMode = !paintMode;

                    if (!paintMode) {
                      isPainted = false;
                    } else {
                      enableScroll = false;
                    }
                  }
                  if (index == 3) {
                    magicMode = !magicMode;

                    if (!magicMode) {
                      isSpelled = false;
                    } else {
                      enableScroll = false;
                    }
                  }
                  if (index == 4) {
                    regionMode = !regionMode;

                    if (!regionMode) {
                      undo();
                    } else {
                      regionPoints = new List<Point<num>>();

                      if (image != null) {
                        imageUpdateRecords
                            .add(ImageHelper.getFullImageByets(image));
                      }

                      enableScroll = false;
                    }
                  }
                  if (index == 5) {
                    undo();
                  }

                  if (index == 6) {
                    changeFilterMode();
                  }

                  if (index == 7) {
                    blurMode = !blurMode;

                    if (!blurMode) {
                      isBlured = false;
                      for (int i = 0; i < image.width; i++) {
                        for (int j = 0; j < image.height; j++) {
                          int imagePixel = imageRGBArray[i][j];
                          if (image.getPixelSafe(i, j) != imagePixel) {
                            image.setPixelRgba(
                                i,
                                j,
                                img.getRed(imagePixel),
                                img.getGreen(imagePixel),
                                img.getBlue(imagePixel));
                          }
                        }
                      }
                    } else {
                      enableScroll = false;
                    }
                  }

                  if (index == 8) {
                    zoomImage(2, context);
                  }

                  if (index == 9) {
                    zoomImage(0.5, context);
                  }

                  if (index == 10) {
                    tapMode = !tapMode;

                    if (tapMode) {
                      regionPoints = new List<Point<num>>();
                      enableScroll = false;
                    }
                  }

                  if (index != 2) {
                    paintMode = false;
                    isPainted = false;
                  }

                  if (index != 3) {
                    magicMode = false;
                    isSpelled = false;
                  }

                  if (index != 4) {
                    if (regionMode) {
                      undo();
                    }

                    regionMode = false;
                  }

                  if (index != 7) {
                    blurMode = false;
                    isBlured = false;
                  }

                  if (index != 10) {
                    tapMode = false;
                  }

                  if (!magicMode &&
                      !paintMode &&
                      !regionMode &&
                      !blurMode &&
                      !tapMode) {
                    enableScroll = true;
                  }

                  setState(() {});
                },
                itemBuilder: (BuildContext context) => <PopupMenuEntry<int>>[
                      const PopupMenuItem<int>(
                        value: 1,
                        child: Text('Color'),
                      ),
                      PopupMenuItem<int>(
                        value: 2,
                        child: Row(
                          children: <Widget>[
                            Text("Paint Mode "),
                            Spacer(),
                            Checkbox(value: paintMode, onChanged: null)
                          ],
                        ),
                      ),
                      PopupMenuItem<int>(
                        value: 3,
                        child: Row(
                          children: <Widget>[
                            Text("Magic Mode "),
                            Spacer(),
                            Checkbox(value: magicMode, onChanged: null)
                          ],
                        ),
                      ),
                      PopupMenuItem<int>(
                        value: 4,
                        child: Row(
                          children: <Widget>[
                            Text("Region Mode "),
                            Spacer(),
                            Checkbox(value: regionMode, onChanged: null)
                          ],
                        ),
                      ),
                      PopupMenuItem<int>(
                        enabled: !regionMode,
                        value: 5,
                        child: Text('Undo'),
                      ),
                      PopupMenuItem<int>(
                          value: 6,
                          child: Row(
                            children: <Widget>[
                              Text("Advanced Filter"),
                              Spacer(),
                              Checkbox(value: anglerFilter, onChanged: null)
                            ],
                          )),
                      PopupMenuItem<int>(
                          value: 7,
                          child: Row(
                            children: <Widget>[
                              Text("Blur Mode"),
                              Spacer(),
                              Checkbox(value: blurMode, onChanged: null)
                            ],
                          )),
                      PopupMenuItem<int>(
                          enabled: !regionMode,
                          value: 8,
                          child: Row(
                            children: <Widget>[
                              Text("Zoom In"),
                              Spacer(),
                              Icon(Icons.zoom_in)
                            ],
                          )),
                      PopupMenuItem<int>(
                          enabled: !regionMode,
                          value: 9,
                          child: Row(
                            children: <Widget>[
                              Text("Zoom Out"),
                              Spacer(),
                              Icon(Icons.zoom_out)
                            ],
                          )),
                      PopupMenuItem<int>(
                          enabled: !regionMode,
                          value: 10,
                          child: Row(
                            children: <Widget>[
                              Text("Tap Mode"),
                              Spacer(),
                              Checkbox(value: tapMode, onChanged: null)
                            ],
                          )),
                    ]),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.developer_board,
              color: Colors.black,
            ),
            title: Container(),
          ),
        ],
        onTap: (int index) {
          if (index == 2) {
            _launchURL('https://www.linkedin.com/in/shafiul-azam-52411a19b/');
          }
        },
      ),
    );
  }
}
