import 'dart:math' as math;
import 'dart:typed_data';
import 'dart:ui';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image/image.dart' as img;
import 'package:image_cropper/image_cropper.dart';
import 'package:color/color.dart' as colr;
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:color_convert/color_convert.dart';
import 'package:flutter/painting.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/services.dart';
import 'package:poly/poly.dart';

class ImageHelper {
  static List<int> getByets(
      context, img.Image image, double zoomOriginX, double zoomOriginY) {
    return img.encodeJpg(img.copyCrop(
        image,
        zoomOriginX.toInt() - 1,
        zoomOriginY.toInt() - 1,
        MediaQuery.of(context).size.width.toInt(),
        (MediaQuery.of(context).size.height * 0.6).toInt()));
  }

  static List<int> getFullImageByets(img.Image image) {
    return img.encodeJpg(image);
  }

  static bool isInsidePoint(double x, double y, List<Point<num>> regionPoints) {
    if (regionPoints.length < 3) {
      return true;
    }

    List<Point<num>> polyPoints = new List<Point<num>>();

    for (int i = 0; i < regionPoints.length; i++) {
      polyPoints.add(math.Point(regionPoints[i].x, regionPoints[i].y));
    }

    polyPoints.add(math.Point(regionPoints[0].x, regionPoints[0].y));

    Polygon polygon = Polygon(polyPoints);

    for (double i = x - 0.3; i < x + 0.3; i += 0.1) {
      for (double j = y - 0.3; j < y + 0.3; j += 0.1) {
        if (polygon.isPointInside(math.Point(i, j))) {
          return true;
        }
      }
    }

    return false;
  }

  static bool isSameColor(int r1, int g1, int b1, int r2, int g2, int b2,
      double allowColorDistance, double givenFilterLevel, bool angleFilter) {
    if (angleFilter) {
      double g1r1Angle = (math.sqrt((r1 * r1) + (g1 * g1)) != 0
          ? g1 / math.sqrt((r1 * r1) + (g1 * g1))
          : 0);
      double b1r1Angle = (math.sqrt((r1 * r1) + (b1 * b1)) != 0
          ? b1 / math.sqrt((r1 * r1) + (b1 * b1))
          : 0);
      double b1g1Angle = (math.sqrt((g1 * g1) + (b1 * b1)) != 0
          ? b1 / math.sqrt((g1 * g1) + (b1 * b1))
          : 0);

      double g2r2Angle = (math.sqrt((r2 * r2) + (g2 * g2)) != 0
          ? g2 / math.sqrt((r2 * r2) + (g2 * g2))
          : 0);
      double b2r2Angle = (math.sqrt((r2 * r2) + (b2 * b2)) != 0
          ? b2 / math.sqrt((r2 * r2) + (b2 * b2))
          : 0);
      double b2g2Angle = (math.sqrt((g2 * g2) + (b2 * b2)) != 0
          ? b2 / math.sqrt((g2 * g2) + (b2 * b2))
          : 0);

      double angleRGDisVal =
          math.sqrt((g1r1Angle - g2r2Angle) * (g1r1Angle - g2r2Angle));
      double angleRBDisVal =
          math.sqrt((b1r1Angle - b2r2Angle) * (b1r1Angle - b2r2Angle));
      double angleBGDisVal =
          math.sqrt((b1g1Angle - b2g2Angle) * (b1g1Angle - b2g2Angle));

      if (angleRGDisVal > (100 - givenFilterLevel) * 0.01 ||
          angleRBDisVal > (100 - givenFilterLevel) * 0.01 ||
          angleBGDisVal > (100 - givenFilterLevel) * 0.01) {
        return false;
      }
    }

    int rDistVal = (r1 - r2) * (r1 - r2);
    int gDistVal = (g1 - g2) * (g1 - g2);
    int bDistVal = (b1 - b2) * (b1 - b2);

    double distance = math.sqrt(rDistVal + gDistVal + bDistVal);

    if (distance <= allowColorDistance * 2.5) {
      return true;
    }

    return false;
  }

  static Future<File> cropImage(filePath, BuildContext context) async {
    File croppedImage = await ImageCropper.cropImage(
        sourcePath: filePath,
        maxWidth: MediaQuery.of(context).size.width.toInt(),
        maxHeight: (MediaQuery.of(context).size.height * 0.6).toInt(),
        aspectRatio: CropAspectRatio(
            ratioX: MediaQuery.of(context).size.width.toInt() * 1.0,
            ratioY: (MediaQuery.of(context).size.height * 0.6).toInt() * 1.0));

    return croppedImage;
  }

  static void saveImage(List<int> byteList) async {
    Uint8List uint8List = Uint8List.fromList(byteList);

    Directory dir = await getApplicationDocumentsDirectory();

    final _random = new math.Random();

    String filePath = dir.path +
        DateTime.now().millisecondsSinceEpoch.toString() +
        _random.nextInt(1000).toString() +
        ".jpg";

    print(filePath);

    File imageFile2 = File(filePath);
    imageFile2.writeAsBytes(uint8List).then((_) {
      GallerySaver.saveImage(imageFile2.path).then((bool success) {
        print(success);
      });
    });
  }

  static Map<String, dynamic> drawRegion(
      double x,
      double y,
      img.Image image,
      var imageRGBArray,
      int currentPolyPoint,
      List<Point<num>> regionPoints,
      int brushSize) {
    for (int i = 0; i < image.width; i++) {
      for (int j = 0; j < image.height; j++) {
        int imagePixel = imageRGBArray[i][j];
        if (image.getPixelSafe(i, j) != imagePixel) {
          image.setPixelRgba(i, j, img.getRed(imagePixel),
              img.getGreen(imagePixel), img.getBlue(imagePixel));
        }
      }
    }

    if (currentPolyPoint == -1)
      return {"image": image, "regionPoints": regionPoints};

    regionPoints[currentPolyPoint] = math.Point(x, y);

    for (int i = 0; i < regionPoints.length; i++) {
      image = img.fillRect(
          image,
          regionPoints[i].x.toInt() - brushSize + 2 < 0
              ? 0
              : regionPoints[i].x.toInt() - brushSize + 2,
          regionPoints[i].y.toInt() - brushSize + 2 < 0
              ? 0
              : regionPoints[i].y.toInt() - brushSize + 2,
          regionPoints[i].x.toInt() + brushSize - 2 > image.width
              ? image.width
              : regionPoints[i].x.toInt() + brushSize - 2,
          regionPoints[i].y.toInt() + brushSize - 2 > image.height
              ? image.height
              : regionPoints[i].y.toInt() + brushSize - 2,
          4286151802);

      if (i > 0) {
        image = img.drawLine(
            image,
            regionPoints[i].x.toInt(),
            regionPoints[i].y.toInt(),
            regionPoints[i - 1].x.toInt(),
            regionPoints[i - 1].y.toInt(),
            4286151802,
            thickness: 3);

        if (i == regionPoints.length - 1 && regionPoints.length > 2) {
          image = img.drawLine(
              image,
              regionPoints[i].x.toInt(),
              regionPoints[i].y.toInt(),
              regionPoints[0].x.toInt(),
              regionPoints[0].y.toInt(),
              4286151802,
              thickness: 3);
        }
      }
    }

    return {"image": image, "regionPoints": regionPoints};
  }

  static Map<String, dynamic> applySpellAt(
      int x,
      int y,
      int brushSize,
      img.Image image,
      var tempImageRGBArray,
      var imageRGBArray,
      List<Point<num>> regionPoints) {
    for (int i = (x - brushSize < 0 ? 0 : x - brushSize);
        i < (x + brushSize > image.width ? image.width : x + brushSize);
        i++) {
      for (int j = (y - brushSize < 0 ? 0 : y - brushSize);
          j < (y + brushSize > image.height ? image.height : y + brushSize);
          j++) {
        bool isOutOfRegion = !isInsidePoint(i * 1.0, j * 1.0, regionPoints);

        if (isOutOfRegion) {
          continue;
        }

        image.setPixelRgba(
            i,
            j,
            img.getRed(tempImageRGBArray[i][j]),
            img.getGreen(tempImageRGBArray[i][j]),
            img.getBlue(tempImageRGBArray[i][j]));

        imageRGBArray[i][j] = tempImageRGBArray[i][j];
      }
    }

    return {"image": image, "imageRGBArray": imageRGBArray};
  }

  static Map<String, dynamic> replaceColorAt(
      int x,
      int y,
      Color currentColor,
      int brushSize,
      img.Image image,
      var tempImageRGBArray,
      var imageRGBArray,
      List<Point<num>> regionPoints,
      double adjustment) {
    int currentColorRed = currentColor.red;
    int currentColorGreen = currentColor.green;
    int currentColorBlue = currentColor.blue;

/*     print(currentColorRed);
    print(currentColorGreen);
    print(currentColorBlue); */

    for (int i = (x - brushSize < 0 ? 0 : x - brushSize);
        i < (x + brushSize >= image.width ? image.width : x + brushSize);
        i++) {
      for (int j = (y - brushSize < 0 ? 0 : y - brushSize);
          j < (y + brushSize >= image.height ? image.height : y + brushSize);
          j++) {
        bool isOutOfRegion = !isInsidePoint(i * 1.0, j * 1.0, regionPoints);

        if (isOutOfRegion) {
          continue;
        }

        var currentColorhslList = convert.rgb
            .hsl(currentColorRed, currentColorGreen, currentColorBlue);

        int realImagePixel = tempImageRGBArray[i][j];

        var realImageColorhslList = convert.rgb.hsl(img.getRed(realImagePixel),
            img.getGreen(realImagePixel), img.getBlue(realImagePixel));

        double lightness = realImageColorhslList[2] + adjustment < 0
            ? 0
            : realImageColorhslList[2] + adjustment > 100
                ? 100
                : realImageColorhslList[2] + adjustment;

        colr.HslColor currentColorhsl = new colr.HslColor(
            currentColorhslList[0], currentColorhslList[1], lightness);

        var currentColorrgbList = convert.hsl
            .rgb(currentColorhsl.h, currentColorhsl.s, currentColorhsl.l);

        image.setPixelRgba(i, j, currentColorrgbList[0], currentColorrgbList[1],
            currentColorrgbList[2]);

        imageRGBArray[i][j] = image.getPixelSafe(i, j);
      }
    }

    return {"image": image, "imageRGBArray": imageRGBArray};
  }

  static Map<String, dynamic> replaceColor(
      int selectedPixel,
      Color currentColor,
      img.Image image,
      var imageRGBArray,
      var tempImageRGBArray,
      List<Point<num>> regionPoints,
      double adjustment,
      double allowDistance,
      double filterLevel,
      bool anglerFilter) {
    int selectedPixelRed = img.getRed(selectedPixel);
    int selectedPixelGreen = img.getGreen(selectedPixel);
    int selectedPixelBlue = img.getBlue(selectedPixel);

/*     print(selectedPixelRed);
    print(selectedPixelGreen);
    print(selectedPixelBlue); */

    int currentColorRed = currentColor.red;
    int currentColorGreen = currentColor.green;
    int currentColorBlue = currentColor.blue;

/*     print(currentColorRed);
    print(currentColorGreen);
    print(currentColorBlue); */

    for (int i = 0; i < image.width; i++) {
      for (int j = 0; j < image.height; j++) {
        int imagePixel = imageRGBArray[i][j];

        bool isOutOfRegion = !isInsidePoint(i * 1.0, j * 1.0, regionPoints);

        if (isOutOfRegion) {
          continue;
        }

        if (isSameColor(
            selectedPixelRed,
            selectedPixelGreen,
            selectedPixelBlue,
            img.getRed(imagePixel),
            img.getGreen(imagePixel),
            img.getBlue(imagePixel),
            allowDistance,
            filterLevel,
            anglerFilter)) {
          var currentColorhslList = convert.rgb
              .hsl(currentColorRed, currentColorGreen, currentColorBlue);

          int realImagePixel = tempImageRGBArray[i][j];

          var realImageColorhslList = convert.rgb.hsl(
              img.getRed(realImagePixel),
              img.getGreen(realImagePixel),
              img.getBlue(realImagePixel));

          double lightness = realImageColorhslList[2] + adjustment < 0
              ? 0
              : realImageColorhslList[2] + adjustment > 100
                  ? 100
                  : realImageColorhslList[2] + adjustment;

          colr.HslColor currentColorhsl = new colr.HslColor(
              currentColorhslList[0], currentColorhslList[1], lightness);

          var currentColorrgbList = convert.hsl
              .rgb(currentColorhsl.h, currentColorhsl.s, currentColorhsl.l);

          image.setPixelRgba(i, j, currentColorrgbList[0],
              currentColorrgbList[1], currentColorrgbList[2]);

          imageRGBArray[i][j] = image.getPixelSafe(i, j);
        }
      }
    }

    return {"image": image, "imageRGBArray": imageRGBArray};
  }

  static Map<String, dynamic> applyGaussianBlurAt(int x, int y, img.Image image,
      var imageRGBArray, List<Point<num>> regionPoints) {
    int x1 = x - 1 < 0 ? 0 : x - 1;
    int y1 = y - 1 < 0 ? 0 : y - 1;

    int x2 = x + 1 > image.width ? image.width : x + 1;
    int y2 = y + 1 > image.height ? image.height : y + 1;

/*     print(image.width);
    print(image.height);
    print(x2);
    print(y2); */

    var blurSource = img.copyCrop(image, x1, y1, 5, 5);

    blurSource = img.gaussianBlur(blurSource, 2);

    for (int i = x1; i <= x2; i++) {
      for (int j = y1; j <= y2; j++) {
        if (!ImageHelper.isInsidePoint(i * 1.0, j * 1.0, regionPoints)) {
          continue;
        }

        image.setPixel(i, j, blurSource.getPixelSafe(i - x1, j - y1));

        imageRGBArray[i][j] = blurSource.getPixelSafe(i - x1, j - y1);
      }
    }

    return {"image": image, "imageRGBArray": imageRGBArray};
  }

  static Map<String, dynamic> zoomImage(
      double scale,
      BuildContext context,
      double zoomOriginX,
      double zoomOriginY,
      List<int> bytes,
      var tempBytes,
      img.Image image,
      img.Image tempImage,
      var imageRGBArray,
      var tempImageRGBArray,
      List<Point<num>> regionPoints) {
    print("hello zoom");

    regionPoints = new List<Point<num>>();

    int width = (image.width * scale).round();
    int height = (image.height * scale).round();

    if ((MediaQuery.of(context).size.height * 0.6).toInt() > height ||
        (MediaQuery.of(context).size.width).toInt() > width) {
      return {
        "zoomOriginX": zoomOriginX,
        "zoomOriginY": zoomOriginY,
        "bytes": bytes,
        "tempBytes": tempBytes,
        "image": image,
        "tempImage": tempImage,
        "imageRGBArray": imageRGBArray,
        "tempImageRGBArray": tempImageRGBArray,
        "regionPoints": regionPoints
      };
    }

    image = img.copyResize(image, width: width, height: height);

    if ((MediaQuery.of(context).size.width).toInt() +
            (zoomOriginX * scale).round() * 1.0 <=
        image.width) {
      zoomOriginX = (zoomOriginX * scale).round() * 1.0;
    } else {
      zoomOriginX =
          image.width - (MediaQuery.of(context).size.width).toInt() * 1.0;
    }

    zoomOriginX = zoomOriginX < 1 ? 1 : zoomOriginX;

    if ((MediaQuery.of(context).size.height * 0.6).toInt() +
            (zoomOriginY * scale).round() * 1.0 <=
        image.height) {
      zoomOriginY = (zoomOriginY * scale).round() * 1.0;
    } else {
      zoomOriginY = image.height -
          (MediaQuery.of(context).size.height * 0.6).toInt() * 1.0;
    }

    zoomOriginY = zoomOriginY < 1 ? 1 : zoomOriginY;

    bytes = img.encodeJpg(image);

    imageRGBArray =
        new List.generate(image.width, (_) => new List(image.height));

    for (int i = 0; i < image.width; i++) {
      for (int j = 0; j < image.height; j++) {
        imageRGBArray[i][j] = image.getPixelSafe(i, j);
      }
    }

    tempImage = img.copyResize(tempImage, width: width, height: height);

    tempBytes = img.encodeJpg(tempImage);

    tempImageRGBArray =
        new List.generate(tempImage.width, (_) => new List(tempImage.height));

    for (int i = 0; i < tempImage.width; i++) {
      for (int j = 0; j < tempImage.height; j++) {
        tempImageRGBArray[i][j] = tempImage.getPixelSafe(i, j);
      }
    }

    //print(zoomOriginY);

    return {
      "zoomOriginX": zoomOriginX,
      "zoomOriginY": zoomOriginY,
      "bytes": bytes,
      "tempBytes": tempBytes,
      "image": image,
      "tempImage": tempImage,
      "imageRGBArray": imageRGBArray,
      "tempImageRGBArray": tempImageRGBArray,
      "regionPoints": regionPoints
    };
  }

  static Map<String, dynamic> changePosition(
      int xDist,
      int yDist,
      BuildContext context,
      double zoomOriginX,
      double zoomOriginY,
      img.Image image) {
    if ((MediaQuery.of(context).size.width).toInt() + zoomOriginX + xDist >
            image.width &&
        xDist > 0) {
      return {"zoomOriginX": zoomOriginX, "zoomOriginY": zoomOriginY};
    }
    if ((MediaQuery.of(context).size.width).toInt() + zoomOriginX + xDist < 1 &&
        xDist < 0) {
      return {"zoomOriginX": zoomOriginX, "zoomOriginY": zoomOriginY};
    }

    if ((MediaQuery.of(context).size.height * 0.6).toInt() +
                zoomOriginY +
                yDist >
            image.height &&
        yDist > 0) {
      return {"zoomOriginX": zoomOriginX, "zoomOriginY": zoomOriginY};
    }
    if ((MediaQuery.of(context).size.height * 0.6).toInt() +
                zoomOriginY +
                yDist <
            1 &&
        yDist < 0) {
      return {"zoomOriginX": zoomOriginX, "zoomOriginY": zoomOriginY};
    }

    double originX = zoomOriginX + xDist;
    double originY = zoomOriginY + yDist;

    originX =
        originX + (MediaQuery.of(context).size.width).toInt() > image.width
            ? (image.width - (MediaQuery.of(context).size.width).toInt()) * 1.0
            : originX;

    zoomOriginX = originX < 1 ? 1 : originX;

    originY = originY + (MediaQuery.of(context).size.height * 0.6).toInt() >
            image.height
        ? (image.height - (MediaQuery.of(context).size.height * 0.6).toInt()) *
            1.0
        : originY;
    zoomOriginY = originY < 1 ? 1 : originY;

    return {"zoomOriginX": zoomOriginX, "zoomOriginY": zoomOriginY};
  }
}
